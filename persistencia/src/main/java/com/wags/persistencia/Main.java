package com.wags.persistencia;

import com.wags.persistencia.controllers.CiudadJpaController;
import com.wags.persistencia.models.Ciudad;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringTokenizer;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Wilmar Giraldo
 */
public class Main {

    private static int lines = 0;
    private static int counter = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Path source = Paths.get("C:\\Program Files\\MySQL\\MySQL Server 5.7\\bin\\ciudad.csv");
        Charset charset = Charset.forName("UTF-8");

        try (BufferedReader reader = Files.newBufferedReader(source, charset)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                lines++;
            }
        } catch (IOException ex) {
            System.err.format("IOException: %s%n", ex);
        }

        try (BufferedReader reader = Files.newBufferedReader(source, charset)) {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.wags_persistencia_jar_1.0-SNAPSHOTPU");
            CiudadJpaController dao_ciudad = new CiudadJpaController(emf);
            String line = null;
            int token = 0;
            int percent = 0;

            while ((line = reader.readLine()) != null) {
                counter++;
                StringTokenizer st = new StringTokenizer(line, "=;");
                Ciudad ciudad = new Ciudad();
                while (st.hasMoreTokens()) {
                    if (token < st.countTokens()) {
                        ciudad.setCcFips((String) st.nextToken());
                        token++;
                    } else {
                        ciudad.setFullNameNd((String) st.nextToken());
                        token = 1;
                    }
                }
                dao_ciudad.create(ciudad);
                percent = (counter * 100) / lines;
                printProgBar(percent);
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    public static void printProgBar(int percent) {
        StringBuilder bar = new StringBuilder("[");

        for (int i = 0; i < 50; i++) {
            if (i < (percent / 2)) {
                bar.append("=");
            } else if (i == (percent / 2)) {
                bar.append(">");
            } else {
                bar.append(" ");
            }
        }

        bar.append("]   ").append(percent).append("%     ");
        System.out.print("\r" + bar.toString());
    }

}
