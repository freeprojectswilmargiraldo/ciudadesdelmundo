/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wags.persistencia.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Wilmar Giraldo
 */
@Entity
@Table(name = "pais")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pais.findAll", query = "SELECT p FROM Pais p"),
    @NamedQuery(name = "Pais.findByPaisId", query = "SELECT p FROM Pais p WHERE p.paisId = :paisId"),
    @NamedQuery(name = "Pais.findByCcFips", query = "SELECT p FROM Pais p WHERE p.ccFips = :ccFips"),
    @NamedQuery(name = "Pais.findByCcIso", query = "SELECT p FROM Pais p WHERE p.ccIso = :ccIso"),
    @NamedQuery(name = "Pais.findByTld", query = "SELECT p FROM Pais p WHERE p.tld = :tld"),
    @NamedQuery(name = "Pais.findByCountryName", query = "SELECT p FROM Pais p WHERE p.countryName = :countryName")})
public class Pais implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pais_id")
    private Integer paisId;
    @Column(name = "CC_FIPS")
    private String ccFips;
    @Column(name = "CC_ISO")
    private String ccIso;
    @Column(name = "TLD")
    private String tld;
    @Column(name = "COUNTRY_NAME")
    private String countryName;

    public Pais() {
    }

    public Pais(Integer paisId) {
        this.paisId = paisId;
    }

    public Integer getPaisId() {
        return paisId;
    }

    public void setPaisId(Integer paisId) {
        this.paisId = paisId;
    }

    public String getCcFips() {
        return ccFips;
    }

    public void setCcFips(String ccFips) {
        this.ccFips = ccFips;
    }

    public String getCcIso() {
        return ccIso;
    }

    public void setCcIso(String ccIso) {
        this.ccIso = ccIso;
    }

    public String getTld() {
        return tld;
    }

    public void setTld(String tld) {
        this.tld = tld;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paisId != null ? paisId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pais)) {
            return false;
        }
        Pais other = (Pais) object;
        if ((this.paisId == null && other.paisId != null) || (this.paisId != null && !this.paisId.equals(other.paisId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wags.persistencia.models.Pais[ paisId=" + paisId + " ]";
    }
    
}
