/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wags.cityweb.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Wilmar Giraldo
 */
@Entity
@Table(name = "subregion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Subregion.findAll", query = "SELECT s FROM Subregion s"),
    @NamedQuery(name = "Subregion.findBySubregionId", query = "SELECT s FROM Subregion s WHERE s.subregionId = :subregionId"),
    @NamedQuery(name = "Subregion.findBySubregion", query = "SELECT s FROM Subregion s WHERE s.subregion = :subregion"),
    @NamedQuery(name = "Subregion.findBySubregionName", query = "SELECT s FROM Subregion s WHERE s.subregionName = :subregionName")})
public class Subregion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "subregion_id")
    private Integer subregionId;
    @Size(max = 5)
    @Column(name = "SUBREGION")
    private String subregion;
    @Size(max = 25)
    @Column(name = "SUBREGION_NAME")
    private String subregionName;

    public Subregion() {
    }

    public Subregion(Integer subregionId) {
        this.subregionId = subregionId;
    }

    public Integer getSubregionId() {
        return subregionId;
    }

    public void setSubregionId(Integer subregionId) {
        this.subregionId = subregionId;
    }

    public String getSubregion() {
        return subregion;
    }

    public void setSubregion(String subregion) {
        this.subregion = subregion;
    }

    public String getSubregionName() {
        return subregionName;
    }

    public void setSubregionName(String subregionName) {
        this.subregionName = subregionName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (subregionId != null ? subregionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Subregion)) {
            return false;
        }
        Subregion other = (Subregion) object;
        if ((this.subregionId == null && other.subregionId != null) || (this.subregionId != null && !this.subregionId.equals(other.subregionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.wags.cityweb.entity.Subregion[ subregionId=" + subregionId + " ]";
    }
    
}
